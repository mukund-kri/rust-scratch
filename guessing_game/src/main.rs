use std::{cmp::Ordering, io};

use rand::Rng;

fn main() {
    println!("Welcome to Guessing Game");

    let target = rand::thread_rng().gen_range(0..=100);

    loop {
        println!("Please enter your Guess ...");
        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess = match guess.trim().parse::<i32>() {
            Ok(guess) => guess,
            Err(_) => {
                println!("Please enter a valid number");
                continue;
            }
        };

        match guess.cmp(&target) {
            Ordering::Less => println!("Too small"),
            Ordering::Greater => println!("Too big"),
            Ordering::Equal => {
                println!("SUCCESS");
                break;
            }
        }
        println!();
    }
}
